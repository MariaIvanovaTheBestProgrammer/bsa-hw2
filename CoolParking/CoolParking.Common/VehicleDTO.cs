﻿using CoolParking.BL.Models;


namespace CoolParking.Common
{
    public class VehicleDTO
    {
        public string Id { get; set; }
        public int VehicleType { get; set; }
        public decimal Balance { get; set; }

        public VehicleDTO()
        {
        }
        public VehicleDTO(string id, int vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public override string ToString()
        {
            return Id + " "+ (VehicleType)VehicleType+ " Balance "+ Balance;
        }
    }
}
