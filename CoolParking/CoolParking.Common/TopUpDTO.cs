﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Common
{
    public class TopUpDTO
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }

        public TopUpDTO(string id, decimal sum)
        {
            Id = id;
            Sum = sum;
        }
    }
}
