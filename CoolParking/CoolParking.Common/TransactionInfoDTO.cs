﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Common
{
    public class TransactionInfoDTO
    {
        public string VehicleId { get; set; }
        public decimal Sum { get; set; }
        public DateTime TransactionDate{ get; set; }
        public override string ToString()
        {
            return TransactionDate + ": " + Sum + " money withdrawn from vehicle with Id='" + VehicleId + "'.";
        }
    }
}
