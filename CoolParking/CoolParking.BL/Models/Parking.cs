﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Lazy<Parking> _instance = new Lazy<Parking>(() => new Parking());
        public decimal Balance { get; set; }
        public List<Vehicle> VehiclesOnParking { get; private set; }
        protected Parking()
        {
            VehiclesOnParking = new List<Vehicle>();
            Balance = 0;
        }
        public static Parking GetInstance() => _instance.Value;
        
        public void Dispose()
        {
            Balance = 0;
            VehiclesOnParking.Clear();
        }
    }
}