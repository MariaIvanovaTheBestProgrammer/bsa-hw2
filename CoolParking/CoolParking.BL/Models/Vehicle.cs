﻿// TODO: implement class Vehicle.
//       +Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       +The format of the identifier is explained in the description of the home task.
//       +Id and VehicleType should not be able for changing.
//       +The Balance should be able to change only in the CoolParking.BL project.
//       +The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       +Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models 
{
    public class Vehicle
    {
        public string Id { get;}
        public VehicleType VehicleType { get;}
        public decimal Balance { get; private set; } 
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!CheckIfIdValid(id))
            {
                throw new ArgumentException("Wrong vehicle Id");
            }
            if (!CheckIfVehicleTypeValid(vehicleType))
            {
                throw new ArgumentException("Wrong vehicle type");
            }
            if (!CheckIfBalanceValid(balance))
            {
                throw new ArgumentException("Balance is under 0");
            }
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        public static bool CheckIfIdValid(string id)
        {
            string pattern = @"^[A-Z]{2}[-]{1}[0-9]{4}[-]{1}[A-Z]{2}$";
            if (!Regex.IsMatch(id, pattern))
            {
                return false;
            }
            return true;
        }
        public static bool CheckIfVehicleTypeValid(VehicleType vehicleType)
        {
            if ((int)vehicleType < 0 || (int)vehicleType > 3)
            {
                return false;
            }
            return true;
        }
        public static bool CheckIfBalanceValid(decimal balance)
        {
            if (balance < 0)
            {
                return false;
            }
            return true;
        }
        public void MoneyWithdraw(decimal sum)
        {
            Balance -= sum;
        }
        public void MoneyTopUp(decimal sum)
        {
            Balance += sum;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            return RandomString(2, random)+'-'+ random.Next(9999).ToString("D4")+ '-'+ RandomString(2, random);
        }
        private static string RandomString(int length, Random random)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public override string ToString()
        {
            return Id + " " + VehicleType + " Balance " + Balance;
        }
    }
}