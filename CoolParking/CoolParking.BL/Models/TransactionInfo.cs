﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public TransactionInfo(decimal sum, string vehicleId)
        {
            Sum = sum;
            TransactionDate = DateTime.Now;
            VehicleId = vehicleId;
        }

        public decimal Sum { get; set; }
        public DateTime TransactionDate { get; set; }
        public string VehicleId { get; set; }

        public override string ToString()
        {
            return TransactionDate + ": "+ Sum + " money withdrawn from vehicle with Id='" + VehicleId+"'.";
        }
    }
}