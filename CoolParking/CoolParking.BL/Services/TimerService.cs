﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Models;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : Interfaces.ITimerService
    {
        public double Interval { get; set; }

        private Timer Timer;

        public TimerService(double interval)
        {
            Interval = interval;
            Timer = new Timer(interval);
        }
        public event ElapsedEventHandler Elapsed 
        { 
            add 
            {
                Timer.Elapsed += value;
            }
            remove 
            {
                Timer.Elapsed -= value;
            } 
        }
        public void Start()
        {
            Timer.Enabled = true;
        }

        public void Stop()
        {
            Timer.Enabled = false;
        }

        public void Dispose()
        {
            Timer.Dispose();
        }
    }
}