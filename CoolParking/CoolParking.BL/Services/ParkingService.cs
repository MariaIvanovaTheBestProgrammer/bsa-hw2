﻿// TODO: implement the ParkingService class from the IParkingService interface.
//      + For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public Parking _parking { get; set; }//private
        private ITimerService withdrawTimerService;
        private ITimerService logTimerService;
        private ILogService logService;
        private List<TransactionInfo> transactionInfos;
        public ParkingService(ITimerService withdrawTimerService, ITimerService logTimerService, ILogService logService)
        {
            _parking = Parking.GetInstance();
            transactionInfos = new List<TransactionInfo>();
            this.withdrawTimerService = withdrawTimerService;
            this.logTimerService = logTimerService;
            this.logService = logService;
            this.withdrawTimerService.Elapsed += OnTimedWithdrawEvent;
            this.logTimerService.Elapsed += OnTimedLogEvent;
            withdrawTimerService.Start();
            logTimerService.Start();
        }
        public void OnTimedWithdrawEvent(object source, ElapsedEventArgs e)
        {
            foreach(Vehicle v in _parking.VehiclesOnParking)
            {
                decimal vehicleTariff = GetVehicleTariff(v);
                decimal payment = CalculatePayment(v, vehicleTariff);
                v.MoneyWithdraw(payment);
                _parking.Balance += payment;
                var transactionInfo1 = new TransactionInfo(payment, v.Id);
                transactionInfos.Add(transactionInfo1);
            }
        }
        public decimal GetVehicleTariff(Vehicle vehicle)
        {
            switch (vehicle.VehicleType)
            {
                case VehicleType.PassengerCar:
                    return Settings.PassengerCarTariff;
                case VehicleType.Truck:
                    return Settings.TruckTariff;
                case VehicleType.Bus:
                    return Settings.BusTariff;
                case VehicleType.Motorcycle:
                    return Settings.MotorcycleTariff;
                default:
                    throw new InvalidOperationException("Unregistered vehicle type");
            }
        }
        public decimal CalculatePayment(Vehicle vehicle, decimal tariff)
        {
            if (vehicle.Balance >= tariff)
            {
                return tariff;
            }
            if(vehicle.Balance < tariff && vehicle.Balance > 0)
            {
                decimal payment = (tariff - vehicle.Balance) * Settings.FineCoefficient + vehicle.Balance;
                return payment;
            }
            else
            {
                decimal payment = tariff * Settings.FineCoefficient;
                return payment;
            }
        }
        public void OnTimedLogEvent(object source, ElapsedEventArgs e)
        {
            if (transactionInfos.Count == 0)
            {
                logService.Write("No transactions");
                return;
            }
            var transactionInfosCopy = new List<TransactionInfo>(transactionInfos);
            foreach ( TransactionInfo t in transactionInfosCopy)
            {
                logService.Write(t.ToString());
            }
            transactionInfos.Clear();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (_parking.VehiclesOnParking.Count == Settings.Capacity)
            {
                throw new InvalidOperationException("No free places on parking");
            }
            Vehicle found = _parking.VehiclesOnParking.FirstOrDefault(e => e.Id == vehicle.Id);
            if (found is not null)
            {
                throw new ArgumentException("Such vehicle already exists");
            }
            _parking.VehiclesOnParking.Add(vehicle);
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return Settings.Capacity - _parking.VehiclesOnParking.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactionInfos.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(_parking.VehiclesOnParking);
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = _parking.VehiclesOnParking.FirstOrDefault(e => e.Id == vehicleId);
            if (vehicle is null)
            {
                throw new ArgumentException($"No such vehicle found with Id = {vehicleId}");
            }
            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException("Balance is under 0");
            }
            _parking.VehiclesOnParking.Remove(vehicle);
        }
        public Vehicle GetVehicleById(string vehicleId)
        {
            Vehicle vehicle = _parking.VehiclesOnParking.FirstOrDefault(e => e.Id == vehicleId);
            if (vehicle is null)
            {
                throw new ArgumentException($"No such vehicle found with Id = {vehicleId}");
            }
            return vehicle;
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle vehicle = _parking.VehiclesOnParking.FirstOrDefault(e => e.Id == vehicleId);
            if (vehicle is null)
            {
                throw new ArgumentException($"Can't top-up, no such vehicle found with Id = {vehicleId}");
            }
            if (sum < 0)
            {
                throw new ArgumentException("ToString up sum is under 0");
            }
            vehicle.MoneyTopUp(sum);
        }

        public void Dispose()
        {
            logTimerService.Dispose();
            withdrawTimerService.Dispose();
            _parking.Dispose();
        }
    }
}