﻿using CoolParking.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Menu.Interfaces
{
    interface ITransactionsHttpService
    {
        Task<IEnumerable<TransactionInfoDTO>> GetLastTransactions();
        Task<string> GetAllTransactionsFromLog();
        Task TopUpVehicle(TopUpDTO topUpDTO);
    }
}
