﻿using CoolParking.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Menu.Interfaces
{
    interface IVehiclesHttpService
    {
        Task<IEnumerable<VehicleDTO>> GetVehicles();
        Task<VehicleDTO> GetVehicleById(string vehicleId);
        Task PutVehicleOnParking(VehicleDTO vehicleDTO);
        Task DeleteVehicleFromParking(string vehicleId);
    }
}
