﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Menu.Interfaces
{
    interface IParkingHttpService
    {
        Task<decimal> GetBalance();
        Task<int> GetCapacity();
        Task<int> GetFreePlaces();
    }
}
