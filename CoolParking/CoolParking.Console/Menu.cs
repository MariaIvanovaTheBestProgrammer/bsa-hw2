﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.Common;
using CoolParking.Menu.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Menu
{
    class Menu
    {
        private ParkingHttpService parkingHttpService;
        private VehiclesHttpService vehiclesHttpService;
        private TransactionsHttpService transactionsHttpService;
        private ParkingService parkingService;
        private LogService logService;
        private TimerService timerService;
        public Menu()
        {
            parkingHttpService = new ParkingHttpService();
            vehiclesHttpService = new VehiclesHttpService();
            transactionsHttpService = new TransactionsHttpService();
            ITimerService withdrawTimerService = new TimerService(BL.Models.Settings.PaymentWriteOffPeriod);
            ITimerService logTimerService = new TimerService(BL.Models.Settings.LogRecordingPeriod);
            logService = new LogService(@"Transactions.log");
            parkingService = new ParkingService(withdrawTimerService, logTimerService, logService);
            timerService = new TimerService(1000);
            timerService.Start();
        }
        public int menu()
        {
            Console.WriteLine("1 - Display the current balance of the Parking: ");
            Console.WriteLine("2 - Display the amount of money earned for the current period (before logging): ");
            Console.WriteLine("3 - Display the number of free / occupied parking spaces: ");
            Console.WriteLine("4 - Display all Parking Transactions for the current period (before being logged): ");
            Console.WriteLine("5 - Print the transaction history (by reading data from the Transactions.log file): ");
            Console.WriteLine("6 - Display the list of vehicles located in the Parking: ");
            Console.WriteLine("7 - Put vehicle on the Parking: ");
            Console.WriteLine("8 - Pick up a vehicle from the Parking: ");
            Console.WriteLine("9 - Top up the balance of a specific vehicle: ");
            Console.WriteLine("99 - exit: ");
            return Convert.ToInt32(Console.ReadLine());
        }
        public async Task DisplayCurrentParkingBalance()
        {
            Console.Clear();
            Console.WriteLine("Current Parking balance: " + await parkingHttpService.GetBalance());
        }
        public async Task DisplayAmountOfMoneyForCurrentPeriod()
        {
            Console.Clear();
            decimal sum = 0;
            foreach(TransactionInfoDTO t in await transactionsHttpService.GetLastTransactions())
            {
                sum += t.Sum;
            }
            Console.WriteLine("Amount of money for current period: " + sum);
        }
        public async Task DisplayFreeAndOccupiedSpaces()
        {
            Console.Clear();
            Console.WriteLine("Free places: " + await parkingHttpService.GetFreePlaces() + " Occupied Places: " + (await parkingHttpService.GetCapacity() - await parkingHttpService.GetFreePlaces()));
        }
        public async Task DisplayTransactionsForCurrentPeriod()
        {
            Console.Clear();
            Console.WriteLine("All transactions for current period: ");
            foreach (TransactionInfoDTO t in await transactionsHttpService.GetLastTransactions())
            {
                Console.WriteLine(t.ToString());
            }
        }
        public async Task DisplayAllTransactionsFromFile()
        {
            Console.Clear();
            Console.WriteLine("All transactions from Transactions.log: ");
            Console.WriteLine(await transactionsHttpService.GetAllTransactionsFromLog());
        }
        public async Task DisplayListOfVehiclesOnParking()
        {
            Console.Clear();
            Console.WriteLine("Vehicles on parking: ");
            foreach (VehicleDTO v in await vehiclesHttpService.GetVehicles())
            { 
                Console.WriteLine(v.Id + " " + v.VehicleType);
            }
        }
        public async Task PutVehicleOnParking()
        {
            Console.Clear();
            try
            {
                Console.WriteLine("Enter vehicle Id in ХХ-YYYY-XX format (where X is any uppercase letter of the English alphabet and Y is any number): ");
                string vId = Console.ReadLine();
                VehicleType vType = new VehicleType();
                Console.WriteLine("Enter 1-4, choose vehicle type: ");
                Console.WriteLine("1 - PassengerCar");
                Console.WriteLine("2 - Truck");
                Console.WriteLine("3 - Bus");
                Console.WriteLine("4 - Motorcycle");
                int num = Convert.ToInt32(Console.ReadLine());
                if (num >= 1 && num <= 4)
                {
                    if (num == 1) vType = VehicleType.PassengerCar;
                    if (num == 2) vType = VehicleType.Truck;
                    if (num == 3) vType = VehicleType.Bus;
                    if (num == 4) vType = VehicleType.Motorcycle;
                }
                else
                {
                    throw new ArgumentException();
                }
                Console.WriteLine("Enter vehicle balance: ");
                decimal vBalance = Convert.ToDecimal(Console.ReadLine());
                await vehiclesHttpService.PutVehicleOnParking(new VehicleDTO(vId, (int)vType, vBalance));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task PickUpVehicleFromParking()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter vehicle Id you want to pick-up from Parking in ХХ-YYYY-XX format (where X is any uppercase letter of the English alphabet and Y is any number): ");
                string vId = Console.ReadLine();
                await vehiclesHttpService.DeleteVehicleFromParking(vId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
        public async Task TopUpBalanceOfSpecificVehicle()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Enter vehicle Id you want to pick-up from Parking in ХХ-YYYY-XX format (where X is any uppercase letter of the English alphabet and Y is any number): ");
                string vId = Console.ReadLine();
                Console.WriteLine("Enter sum: ");
                decimal vSum = Convert.ToDecimal(Console.ReadLine());
                await transactionsHttpService.TopUpVehicle(new TopUpDTO(vId, vSum));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.Write("Press Eneter key to continue.. ");
                Console.ReadLine();
            }
        }
    }
}
