﻿using CoolParking.Common;
using CoolParking.Menu.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Menu.Services
{
    public class ParkingHttpService : IParkingHttpService
    {
        static readonly HttpClient client = new HttpClient();
        public async Task<decimal> GetBalance()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "parking/balance");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<decimal>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return 0;
            }
        }

        public async Task<int> GetCapacity()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "parking/capacity");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return 0;
            }
        }

        public async Task<int> GetFreePlaces()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "parking/freePlaces");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<int>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return 0;
            }
        }
    }
}
