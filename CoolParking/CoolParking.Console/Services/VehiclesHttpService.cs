﻿using CoolParking.Common;
using CoolParking.Menu.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Menu.Services
{
    public class VehiclesHttpService : IVehiclesHttpService
    {
        static readonly HttpClient client = new HttpClient();

        public async Task<IEnumerable<VehicleDTO>> GetVehicles()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "vehicles");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<VehicleDTO>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }

        public async Task<VehicleDTO> GetVehicleById(string vehicleId)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + $"vehicles/{vehicleId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<VehicleDTO>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }

        public async Task PutVehicleOnParking(VehicleDTO vehicleDTO)
        {
            try
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(vehicleDTO), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(Settings.basePath+"vehicles", stringContent);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }

        public async Task DeleteVehicleFromParking(string vehicleId)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync(Settings.basePath + $"vehicles/{vehicleId}");
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
    }
}
