﻿using CoolParking.Common;
using CoolParking.Menu.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Menu.Services
{
    public class TransactionsHttpService : ITransactionsHttpService
    {
        static readonly HttpClient client = new HttpClient();
        
        public async Task<string> GetAllTransactionsFromLog()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "transactions/all");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<string>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }

        public async Task<IEnumerable<TransactionInfoDTO>> GetLastTransactions()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "transactions/last");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<TransactionInfoDTO>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }

        public async Task TopUpVehicle(TopUpDTO topUpDTO)
        {
            try
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(topUpDTO), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync(Settings.basePath + "transactions/topUpVehicle", stringContent);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
    }
}
