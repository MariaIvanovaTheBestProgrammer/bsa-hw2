﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using System;
using System.Threading.Tasks;

namespace CoolParking.Menu
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Menu menu = new Menu();
            int op;
            do
            {
                op = menu.menu();
                if (op == 1)
                {
                    await menu.DisplayCurrentParkingBalance();
                }
                if (op == 2)
                {
                    await menu.DisplayAmountOfMoneyForCurrentPeriod();
                }
                if (op == 3)
                {
                    await menu.DisplayFreeAndOccupiedSpaces();
                }
                if (op == 4)
                {
                    await menu.DisplayTransactionsForCurrentPeriod();
                }
                if (op == 5)
                {
                    await menu.DisplayAllTransactionsFromFile();
                }
                if (op == 6)
                {
                    await menu.DisplayListOfVehiclesOnParking();
                }
                if (op == 7)
                {
                    await menu.PutVehicleOnParking();
                }
                if (op == 8)
                {
                    await menu.PickUpVehicleFromParking();
                }
                if (op == 9)
                {
                    await menu.TopUpBalanceOfSpecificVehicle();
                }
            } while (op != 99);
        }
    }
}
