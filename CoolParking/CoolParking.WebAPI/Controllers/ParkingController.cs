﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("balance")]
        public decimal GetBalance()
        {
            return _parkingService.GetBalance();
        }
        [HttpGet("capacity")]
        public int GetCapacity()
        {
            return _parkingService.GetCapacity();
        }
        [HttpGet("freePlaces")]
        public int GetFreePlaces()
        {
            return _parkingService.GetFreePlaces();
        }

    }
}
