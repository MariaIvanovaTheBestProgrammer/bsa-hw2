﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionInfoDTO>> GetLastTransactions()
        {
            var transactionInfoDTOs = from t in _parkingService.GetLastParkingTransactions()
                              select new TransactionInfoDTO()
                              {
                                  TransactionDate = t.TransactionDate,
                                  Sum = t.Sum,
                                  VehicleId = t.VehicleId
                              };
            return Ok(transactionInfoDTOs);
        }
        [HttpGet("all")]
        public ActionResult<string> GetAllTransactionsFromLog()
        {
            try
            {
                return Ok(_parkingService.ReadFromLog());
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
        [HttpPut("topUpVehicle")]
        public ActionResult TopUpVehicle(TopUpDTO topUpDTO)
        {
            try
            {
                if (!Vehicle.CheckIfIdValid(topUpDTO.Id))
                {
                    return BadRequest("Wrong vehicle id");
                }
                if (topUpDTO.Sum<0)
                {
                    return BadRequest("Top up sum is under 0");
                }
                _parkingService.TopUpVehicle(topUpDTO.Id, topUpDTO.Sum);
                Vehicle vehicle = _parkingService.GetVehicleById(topUpDTO.Id);
                return Ok(new VehicleDTO(vehicle.Id, (int)vehicle.VehicleType, vehicle.Balance));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
