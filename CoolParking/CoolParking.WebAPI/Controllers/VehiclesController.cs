﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        [HttpGet]
        public ActionResult<IEnumerable<VehicleDTO>> GetVehicles()
        {
            //return _parkingService.GetVehicles();
            var vehicleDTOs =  from v in _parkingService.GetVehicles()
                   select new VehicleDTO()
                   {
                       Id = v.Id,
                       VehicleType = (int)v.VehicleType,
                       Balance = v.Balance
                    };
            return Ok(vehicleDTOs);
        }
        [HttpGet("{vehicleId}")]
        public ActionResult<VehicleDTO> GetVehicleById(string vehicleId)
        {
            try
            {
                if (!Vehicle.CheckIfIdValid(vehicleId))
                {
                    return BadRequest("Wrong vehicle Id");
                } 
                var vehicle = _parkingService.GetVehicleById(vehicleId);
                var vehicleDTO = new VehicleDTO(vehicle.Id, (int)vehicle.VehicleType, vehicle.Balance);
                return vehicleDTO;
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
        [HttpPost]
        public IActionResult PutVehicleOnParking(VehicleDTO vehicleDTO)
        {
            try
            {
                Vehicle vehicle = new Vehicle(vehicleDTO.Id, (VehicleType)vehicleDTO.VehicleType, vehicleDTO.Balance);
                _parkingService.AddVehicle(vehicle);
                return StatusCode(201, vehicle);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        [HttpDelete("{vehicleId}")]
        public IActionResult DeleteVehicleFromParking(string vehicleId)
        {
            try
            {
                if (!Vehicle.CheckIfIdValid(vehicleId))
                {
                    return BadRequest("Wrong vehicle Id");
                }
                _parkingService.RemoveVehicle(vehicleId);
                return NoContent();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }

    }
}
